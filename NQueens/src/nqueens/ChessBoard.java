/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nqueens;

/**
 *
 * @author Peter
 * @version 1.0
 * Student Number : 5253133
 */
public class ChessBoard {

    public String[][] board;
    private int[][] queens; 
    private int size;
    
    public ChessBoard (int n)
    {
        size = n;
        queens = new int[size][2];
    }
    
    public void Print()
    {
        //clear the board
        board = new String[size][size];
        
        //add all the queens to the board
        for(int i = 0; i < size; i++)
            board[queens[i][0]][queens[i][1]] = ""+(i+1);
        
        //print the board
        for(int i = 0; i < size; i++)
        {
            for(int j = 0; j < size; j++)
            {
                if(board[i][j] != null && !board[i][j].equals(""))
                    System.out.print("["+board[i][j]+"]");
                else
                    System.out.print("[-]");
            }
            System.out.println();
        }
    }
    
    public int checkPiece(int peice)
    {
        int deltaRow;
        int deltaCol;
        int hitCount = 0;
        
        for(int currentPeice = 0; currentPeice < size; currentPeice ++)
        {
            //skip if peice is the current peice checking
            if(currentPeice == peice) continue;
            
            //check the delta between rows and columns
            deltaRow = queens[peice][0] - queens[currentPeice][0];
            deltaCol = queens[peice][1] - queens[currentPeice][1];
            
            //on the same row
            if(deltaRow == 0) hitCount++;
            //on the same column
            else if(deltaCol == 0) hitCount++;
            //on the same diagonal
            else if(Math.abs(deltaRow) == Math.abs(deltaCol)) hitCount++;      
        }
        
        return hitCount;
    }

}
